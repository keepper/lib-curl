<?php
namespace Keepper\Lib\Curl;

use Keepper\Lib\Curl\Error\CurlError;
use Keepper\Lib\Curl\Exceptions\CurlException;
use Keepper\Lib\Curl\Exceptions\IllegalOperation;
use Keepper\Lib\Curl\Error\CurlErrorInterface;
use Keepper\Lib\Curl\Option\Option;
use Keepper\Lib\Curl\Option\OptionValidator;
use Keepper\Lib\Curl\Option\OptionValidatorInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class Curl implements CurlInterface {

	use LoggerAwareTrait;

	private $descriptor;
	private $optionValidator;

	public function __construct(
		OptionValidatorInterface $optionValidator = null,
		LoggerInterface $logger = null
	) {
		$this->optionValidator = $optionValidator ?? new OptionValidator();
		$this->setLogger($logger ?? new NullLogger());
	}

	private function throwIfNotInitDescriptor() {
		if ( is_null($this->descriptor) ) {
			throw new IllegalOperation('Вызов зависимого метода без инициализации дескриптора');
		}
	}

	/**
	 * @inheritdoc
	 */
	public function init(string $url = null): CurlInterface {
		$this->close();
		$this->descriptor = $this->curl_init($url);
		return $this;
	}

	protected function curl_init(string $url = null) {
		$this->logger->debug('Lib\Curl run function curl_init('.$url.')');
		return curl_init($url);
	}

	/**
	 * @return CurlInterface
	 */
	public function close(): CurlInterface {
		if ( !is_null($this->descriptor) ) {
			$this->curl_close($this->descriptor);
		}
		return $this;
	}

	protected function curl_close($descriptor) {
		$this->logger->debug('Lib\Curl run function curl_close()');
		return curl_init($descriptor);
	}

	/**
	 * @inheritdoc
	 */
	public function setOption(int $option, $value): bool {
		$this->throwIfNotInitDescriptor();
		$this->optionValidator->throwIsOptionValueInvalid($option, $value);
		return $this->curl_setopt($this->descriptor, $option, $value);
	}

	protected function curl_setopt($descriptor, $option, $value) {
		$optionName = isset(Option::MAP[$option]) ? Option::MAP[$option] : $option;
		$this->logger->debug('Lib\Curl run function curl_setopt('.$optionName.', '.$value.')');
		return curl_setopt($descriptor, $option, $value);
	}

	/**
	 * @inheritdoc
	 */
	public function escape(string $str): string {
		$this->throwIfNotInitDescriptor();
		return $this->curl_escape($this->descriptor, $str);
	}

	protected function curl_escape($descriptor, string $str) {
		$this->logger->debug('Lib\Curl run function curl_escape('.$str.')');
		return curl_escape($descriptor, $str);
	}

	/**
	 * @inheritdoc
	 */
	public function unescape(string $str): string {
		$this->throwIfNotInitDescriptor();
		return $this->curl_unescape($this->descriptor, $str);
	}

	protected function curl_unescape($descriptor, string $str) {
		$this->logger->debug('Lib\Curl run function curl_unescape('.$str.')');
		return curl_unescape($descriptor, $str);
	}

	/**
	 * @inheritdoc
	 */
	public function error(): ?CurlErrorInterface {
		$this->throwIfNotInitDescriptor();
		$code = $this->curl_errno($this->descriptor);
		if ( $code == 0 || is_null($code) ) {
			return null;
		}
		$message = $this->curl_error($this->descriptor);
		$codeMessage = $this->curl_strerror($code);
		return new CurlError($code, $message, $codeMessage);
	}

	protected function curl_errno($descriptor) {
		$this->logger->debug('Lib\Curl run function curl_errno()');
		return curl_errno($descriptor);
	}

	protected function curl_error($descriptor) {
		$this->logger->debug('Lib\Curl run function curl_error()');
		return curl_error($descriptor);
	}

	protected function curl_strerror($code) {
		$this->logger->debug('Lib\Curl run function curl_strerror('.$code.')');
		return curl_strerror($code);
	}

	/**
	 * @inheritdoc
	 */
	public function setOptions(array $options): bool {
		$this->throwIfNotInitDescriptor();
		$result = true;
		foreach ($options as $option => $value) {
			$result = $result && $this->setOption($option, $value);
		}
		return $result;
	}

	/**
	 * @inheritdoc
	 */
	public function reset(): CurlInterface {
		if ( is_null($this->descriptor) ) {
			return $this;
		}

		$this->curl_reset($this->descriptor);

		return $this;
	}

	protected function curl_reset($descriptor) {
		$this->logger->debug('Lib\Curl run function curl_reset()');
		return curl_reset($descriptor);
	}

	/**
	 * @inheritdoc
	 */
	public function exec(): string {
		$this->throwIfNotInitDescriptor();

		$result = $this->curl_exec($this->descriptor);

		if ($result === false) {
			$error = $this->error();
			$message = is_null($error) ? 'Выполнение curl оперции не удалось' : $error->message();
			throw new CurlException($message);
		}

		if ($result === true) {
			// Согласно документации true будет при успешной операции, но не выставленой опции CURLOPT_RETURNTRANSFER
			return '';
		}
		return $result;
	}

	protected function curl_exec($descriptor) {
		$this->logger->debug('Lib\Curl run function curl_exec()');
		return curl_exec($descriptor);
	}

	/**
	 * @inheritdoc
	 */
	public function getInfo(int $option) {
		$this->throwIfNotInitDescriptor();
		return $this->curl_getinfo($this->descriptor, $option);
	}

	protected function curl_getinfo($descriptor, $option) {
		$this->logger->debug('Lib\Curl run function curl_getinfo('.$option.')');
		return curl_getinfo($descriptor, $option);
	}
}
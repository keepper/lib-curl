<?php
namespace Keepper\Lib\Curl;

use Keepper\Lib\Curl\Error\CurlErrorInterface;
use Keepper\Lib\Curl\Exceptions\CurlException;
use Keepper\Lib\Curl\Exceptions\IllegalOperation;
use Keepper\Lib\Curl\Exceptions\IllegalOptionValueException;

interface CurlInterface {

	/**
	 * Инициализирует новый дескриптор cURL,
	 * если ранее был открыт иной дескриптор, закрывает его
	 * @see curl_init
	 * @param string|null $url  Если указан, опция CURLOPT_URL будет автоматически установлена в значение этого аргумента.
	 * @return CurlInterface
	 */
	public function init(string $url = null): CurlInterface;

	/**
	 * Устанавливает параметр для сеанса CURL
	 * @param int $option   Устанавливаемый параметр CURLOPT_XXX.
	 * @param mixed $value  Значение параметра option
	 * @return bool
	 * @throws IllegalOptionValueException
	 * @throws IllegalOperation
	 */
	public function setOption(int $option, $value): bool;

	/**
	 * Устанавливает несколько параметров для сеанса cURL
	 * @param array $options   Массив (array), определяющий устанавливаемые параметры и их значения. Ключи должны быть корректными константами для функции curl_setopt() или их целочисленными эквивалентами.
	 * @return bool Возвращает TRUE, если все параметры были успешно установлены.
	 * @throws IllegalOptionValueException
	 * @throws IllegalOperation
	 */
	public function setOptions(array $options): bool;

	/**
	 * Выполняет запрос cURL
	 * @see curl_exec()
	 * @return string Если установлена опция CURLOPT_RETURNTRANSFER, при успешном завершении будет возвращен результат, если опция не установлена будет возвращена пустая строка
	 * @throws CurlException При не удаче будет сгенерировано исключение
	 * @throws IllegalOperation
	 */
	public function exec(): string;

	/**
	 * Закрываетранее открытых хэндлер curl
	 * @see curl_close()
	 * @return CurlInterface
	 */
	public function close(): CurlInterface;

	/**
	 * Сбросить все настройки обработчика сессии libcurl
	 * @see curl_reset()
	 * @return CurlInterface
	 */
	public function reset(): CurlInterface;

	/**
	 * Кодирует заданную строку как URL
	 * @see curl_escape()
	 * @param string $str
	 * @return string
	 * @throws IllegalOperation
	 */
	public function escape(string $str): string;

	/**
	 * Декодирует закодированную URL-строку
	 * @see curl_unescape()
	 * @param string $str
	 * @return string
	 * @throws IllegalOperation
	 */
	public function unescape(string $str): string;

	/**
	 * Возвращает информацию о последней ошибке, если она была
	 * Аналог функций @see curl_errno() @see curl_error()
	 * @return CurlErrorInterface|null
	 * @throws IllegalOperation
	 */
	public function error(): ?CurlErrorInterface;

	/**
	 * Возвращает информацию об определенной операции
	 * @see curl_getinfo()
	 * @param int $option
	 * @return mixed
	 * @throws IllegalOperation
	 */
	public function getInfo(int $option);
}
<?php
namespace Keepper\Lib\Curl\Error;

class CurlError implements CurlErrorInterface {

	private $code;
	private $message;
	private $messageByCode;

	public function __construct(int $code, string $message, string $messageByCode = '') {
		$this->code = $code;
		$this->message = $message;
		$this->messageByCode = $messageByCode;
	}

	/**
	 * @inheritdoc
	 */
	public function code(): int {
		return $this->code;
	}

	/**
	 * @inheritdoc
	 */
	public function messageByCode(): string {
		return $this->messageByCode;
	}

	/**
	 * @inheritdoc
	 */
	public function message(): string {
		return $this->message;
	}
}
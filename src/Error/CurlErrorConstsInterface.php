<?php
namespace Keepper\Lib\Curl\Error;

interface CurlErrorConstsInterface {
    /**
     * The URL you passed to libcurl used a protocol that this libcurl does not support. The support might be a
     * compile-time option that you didn't use, it can be a misspelled protocol string or just a protocol libcurl
     * has no code for.
     * @see CURLE_UNSUPPORTED_PROTOCOL
     */
    const UNSUPPORTED_PROTOCOL = 1;

    /**
     * Very early initialization code failed. This is likely to be an internal error or problem, or a resource problem
     * where something fundamental couldn't get done at init time.
     * @see CURLE_FAILED_INIT (2)
     */
    const FAILED_INIT = 2;

    /**
     * The URL was not properly formatted.
     * @see CURLE_URL_MALFORMAT (3)
     */
    const URL_MALFORMAT = 3;

    /**
     * A requested feature, protocol or option was not found built-in in this libcurl due to a build-time decision.
     * This means that a feature or option was not enabled or explicitly disabled when libcurl was built and in order
     * to get it to function you have to get a rebuilt libcurl.
     * @see CURLE_NOT_BUILT_IN (4)
     */
    const NOT_BUILT_IN = 4;

    /**
     * Couldn't resolve proxy. The given proxy host could not be resolved.
     * @see CURLE_COULDNT_RESOLVE_PROXY (5)
     */
    const COULDNT_RESOLVE_PROXY = 5;

    /**
     * Couldn't resolve host. The given remote host was not resolved.
     * @see CURLE_COULDNT_RESOLVE_HOST (6)
     */
    const COULDNT_RESOLVE_HOST = 6;

    /**
     * Failed to connect() to host or proxy.
     * @see CURLE_COULDNT_CONNECT (7)
     */
    const COULDNT_CONNECT = 7;

    /**
     * The server sent data libcurl couldn't parse. This error code is used for more than just FTP and is aliased as
     * @see CURLE_WEIRD_SERVER_REPLY since 7.51.0.
     * @see CURLE_FTP_WEIRD_SERVER_REPLY (8)
     */
    const FTP_WEIRD_SERVER_REPLY = 8;

    /**
     * We were denied access to the resource given in the URL. For FTP, this occurs while trying to change to the remote directory.
     * @see CURLE_REMOTE_ACCESS_DENIED (9)
     */
    const REMOTE_ACCESS_DENIED = 9;

    /**
     * While waiting for the server to connect back when an active FTP session is used, an error code was sent over the
     * control connection or similar.
     * @see CURLE_FTP_ACCEPT_FAILED (10)
     */
    const FTP_ACCEPT_FAILED = 10;

    /**
     * After having sent the FTP password to the server, libcurl expects a proper reply. This error code indicates
     * that an unexpected code was returned.
     * @see CURLE_FTP_WEIRD_PASS_REPLY (11)
     */
    const FTP_WEIRD_PASS_REPLY = 11;

    /**
     * During an active FTP session while waiting for the server to connect,
     * the @see CURLOPT_ACCEPTTIMEOUT_MS (or the internal default) timeout expired.
     * @see CURLE_FTP_ACCEPT_TIMEOUT (12)
     */
    const FTP_ACCEPT_TIMEOUT = 12;

    /**
     * libcurl failed to get a sensible result back from the server as a response to either a PASV or a EPSV command.
     * The server is flawed.
     * @see CURLE_FTP_WEIRD_PASV_REPLY (13)
     */
    const FTP_WEIRD_PASV_REPLY = 13;

    /**
     * FTP servers return a 227-line as a response to a PASV command. If libcurl fails to parse that line, this return
     * code is passed back.
     * @see CURLE_FTP_WEIRD_227_FORMAT (14)
     */
    const FTP_WEIRD_227_FORMAT = 14;

    /**
     * An internal failure to lookup the host used for the new connection.
     * @see CURLE_FTP_CANT_GET_HOST (15)
     */
    const FTP_CANT_GET_HOST = 15;

    /**
     * A problem was detected in the HTTP2 framing layer. This is somewhat generic and can be one out of several
     * problems, see the error buffer for details.
     * @see CURLE_HTTP2 (16)
     */
    const HTTP2 = 16;

    /**
     * Received an error when trying to set the transfer mode to binary or ASCII.
     * @see CURLE_FTP_COULDNT_SET_TYPE (17)
     */
    const FTP_COULDNT_SET_TYPE = 17;

    /**
     * A file transfer was shorter or larger than expected. This happens when the server first reports an expected
     * transfer size, and then delivers data that doesn't match the previously given size.
     * @see CURLE_PARTIAL_FILE (18)
     */
    const PARTIAL_FILE = 18;

    /**
     * This was either a weird reply to a 'RETR' command or a zero byte transfer complete.
     * @see CURLE_FTP_COULDNT_RETR_FILE (19)
     */
    const FTP_COULDNT_RETR_FILE = 19;

    /**
     * When sending custom "QUOTE" commands to the remote server, one of the commands returned an error code that was
     * 400 or higher (for FTP) or otherwise indicated unsuccessful completion of the command.
     * @see CURLE_QUOTE_ERROR (21)
     */
    const QUOTE_ERROR = 21;

    /**
     * This is returned if CURLOPT_FAILONERROR is set TRUE and the HTTP server returns an error code that is >= 400.
     * @see CURLE_HTTP_RETURNED_ERROR (22)
     */
    const HTTP_RETURNED_ERROR = 22;

    /**
     * An error occurred when writing received data to a local file, or an error was returned to libcurl from a
     * write callback.
     * @see CURLE_WRITE_ERROR (23)
     */
    const WRITE_ERROR = 23;

    /**
     * Failed starting the upload. For FTP, the server typically denied the STOR command. The error buffer usually
     * contains the server's explanation for this.
     * @see CURLE_UPLOAD_FAILED (25)
     */
    const UPLOAD_FAILED = 25;

    /**
     * There was a problem reading a local file or an error returned by the read callback.
     * @see CURLE_READ_ERROR (26)
     */
    const READ_ERROR = 26;

    /**
     * A memory allocation request failed. This is serious badness and things are severely screwed up if this ever occurs.
     * @see CURLE_OUT_OF_MEMORY (27)
     */
    const OUT_OF_MEMORY = 27;

    /**
     * Operation timeout. The specified time-out period was reached according to the conditions.
     * @see CURLE_OPERATION_TIMEDOUT (28)
     */
    const OPERATION_TIMEDOUT = 28;

    /**
     * The FTP PORT command returned error. This mostly happens when you haven't specified a good enough
     * address for libcurl to use. See CURLOPT_FTPPORT.
     * @see CURLE_FTP_PORT_FAILED (30)
     */
    const FTP_PORT_FAILED = 30;

    /**
     * The FTP REST command returned error. This should never happen if the server is sane.
     * @see CURLE_FTP_COULDNT_USE_REST (31)
     */
    const FTP_COULDNT_USE_REST = 31;

    /**
     * The server does not support or accept range requests.
     * @see CURLE_RANGE_ERROR (33)
     */
    const RANGE_ERROR = 33;

    /**
     * This is an odd error that mainly occurs due to internal confusion.
     * @see CURLE_HTTP_POST_ERROR (34)
     */
    const HTTP_POST_ERROR = 34;

    /**
     * A problem occurred somewhere in the SSL/TLS handshake. You really want the error buffer and read the message
     * there as it pinpoints the problem slightly more. Could be certificates (file formats, paths, permissions),
     * passwords, and others.
     * @see CURLE_SSL_CONNECT_ERROR (35)
     */
    const SSL_CONNECT_ERROR = 35;

    /**
     * The download could not be resumed because the specified offset was out of the file boundary.
     * @see CURLE_BAD_DOWNLOAD_RESUME (36)
     */
    const BAD_DOWNLOAD_RESUME = 36;

    /**
     * A file given with FILE:// couldn't be opened. Most likely because the file path doesn't identify an existing file.
     * Did you check file permissions?
     * @see CURLE_FILE_COULDNT_READ_FILE (37)
     */
    const FILE_COULDNT_READ_FILE = 37;

    /**
     * LDAP cannot bind. LDAP bind operation failed.
     * @see CURLE_LDAP_CANNOT_BIND (38)
     */
    const LDAP_CANNOT_BIND = 38;

    /**
     * LDAP search failed.
     * @see CURLE_LDAP_SEARCH_FAILED (39)
     */
    const LDAP_SEARCH_FAILED = 39;

    /**
     * Function not found. A required zlib function was not found.
     * @see CURLE_FUNCTION_NOT_FOUND (41)
     */
    const FUNCTION_NOT_FOUND = 41;

    /**
     * Aborted by callback. A callback returned "abort" to libcurl.
     * @see CURLE_ABORTED_BY_CALLBACK (42)
     */
    const ABORTED_BY_CALLBACK = 42;

    /**
     * Internal error. A function was called with a bad parameter.
     * @see CURLE_BAD_FUNCTION_ARGUMENT (43)
     */
    const BAD_FUNCTION_ARGUMENT = 43;

    /**
     * Interface error. A specified outgoing interface could not be used. Set which interface to use for outgoing
     * connections' source IP address with @see CURLOPT_INTERFACE.
     * @see CURLE_INTERFACE_FAILED (45)
     */
    const INTERFACE_FAILED = 45;

    /**
     * Too many redirects. When following redirects, libcurl hit the maximum amount.
     * Set your limit with @see CURLOPT_MAXREDIRS.
     * @see CURLE_TOO_MANY_REDIRECTS (47)
     */
    const TOO_MANY_REDIRECTS = 47;

    /**
     * An option passed to libcurl is not recognized/known. Refer to the appropriate documentation. This is most likely
     * a problem in the program that uses libcurl. The error buffer might contain more specific information about
     * which exact option it concerns.
     * @see CURLE_UNKNOWN_OPTION (48)
     */
    const UNKNOWN_OPTION = 48;

    /**
     * A telnet option string was Illegally formatted.
     * @see CURLE_TELNET_OPTION_SYNTAX (49)
     */
    const TELNET_OPTION_SYNTAX = 49;

    /**
     * Nothing was returned from the server, and under the circumstances, getting nothing is considered an error.
     * @see CURLE_GOT_NOTHING (52)
     */
    const GOT_NOTHING = 52;

    /**
     * The specified crypto engine wasn't found.
     * @see CURLE_SSL_ENGINE_NOTFOUND (53)
     */
    const SSL_ENGINE_NOTFOUND = 53;

    /**
     * Failed setting the selected SSL crypto engine as default!
     * @see CURLE_SSL_ENGINE_SETFAILED (54)
     */
    const SSL_ENGINE_SETFAILED = 54;

    /**
     * Failed sending network data.
     * @see CURLE_SEND_ERROR (55)
     */
    const SEND_ERROR = 55;

    /**
     * Failure with receiving network data.
     * @see CURLE_RECV_ERROR (56)
     */
    const RECV_ERROR = 56;

    /**
     * problem with the local client certificate.
     * @see CURLE_SSL_CERTPROBLEM (58)
     */
    const SSL_CERTPROBLEM = 58;

    /**
     * Couldn't use specified cipher.
     * @see CURLE_SSL_CIPHER (59)
     */
    const SSL_CIPHER = 59;

    /**
     * The remote server's SSL certificate or SSH md5 fingerprint was deemed not OK.
     * This error code has been unified with @see CURLE_SSL_CACERT since 7.62.0. Its previous value was 51.
     * @see CURLE_PEER_FAILED_VERIFICATION (60)
     */
    const PEER_FAILED_VERIFICATION = 60;

    /**
     * Unrecognized transfer encoding.
     * @see CURLE_BAD_CONTENT_ENCODING (61)
     */
    const BAD_CONTENT_ENCODING = 61;

    /**
     * Invalid LDAP URL.
     * @see CURLE_LDAP_INVALID_URL (62)
     */
    const LDAP_INVALID_URL = 62;

    /**
     * Maximum file size exceeded.
     * @see CURLE_FILESIZE_EXCEEDED (63)
     */
    const FILESIZE_EXCEEDED = 63;

    /**
     * Requested FTP SSL level failed.
     * @see CURLE_USE_SSL_FAILED (64)
     */
    const USE_SSL_FAILED = 64;

    /**
     * When doing a send operation curl had to rewind the data to retransmit, but the rewinding operation failed.
     * @see CURLE_SEND_FAIL_REWIND (65)
     */
    const SEND_FAIL_REWIND = 65;

    /**
     * Initiating the SSL Engine failed.
     * @see CURLE_SSL_ENGINE_INITFAILED (66)
     */
    const SSL_ENGINE_INITFAILED = 66;

    /**
     * The remote server denied curl to login (Added in 7.13.1)
     * @see CURLE_LOGIN_DENIED (67)
     */
    const LOGIN_DENIED = 67;

    /**
     * File not found on TFTP server.
     * @see CURLE_TFTP_NOTFOUND (68)
     */
    const TFTP_NOTFOUND = 68;

    /**
     * Permission problem on TFTP server.
     * @see CURLE_TFTP_PERM (69)
     */
    const TFTP_PERM = 69;

    /**
     * Out of disk space on the server.
     * @see CURLE_REMOTE_DISK_FULL (70)
     */
    const REMOTE_DISK_FULL = 70;

    /**
     * Illegal TFTP operation.
     * @see CURLE_TFTP_ILLEGAL (71)
     */
    const TFTP_ILLEGAL = 71;

    /**
     * Unknown TFTP transfer ID.
     * @see CURLE_TFTP_UNKNOWNID (72)
     */
    const TFTP_UNKNOWNID = 72;

    /**
     * File already exists and will not be overwritten.
     * @see CURLE_REMOTE_FILE_EXISTS (73)
     */
    const REMOTE_FILE_EXISTS = 73;

    /**
     * This error should never be returned by a properly functioning TFTP server.
     * @see CURLE_TFTP_NOSUCHUSER (74)
     */
    const TFTP_NOSUCHUSER = 74;

    /**
     * Character conversion failed.
     * @see CURLE_CONV_FAILED (75)
     */
    const CONV_FAILED = 75;

    /**
     * Caller must register conversion callbacks.
     * @see CURLE_CONV_REQD (76)
     */
    const CONV_REQD = 76;

    /**
     * Problem with reading the SSL CA cert (path? access rights?)
     * @see CURLE_SSL_CACERT_BADFILE (77)
     */
    const SSL_CACERT_BADFILE = 77;

    /**
     * The resource referenced in the URL does not exist.
     * @see CURLE_REMOTE_FILE_NOT_FOUND (78)
     */
    const REMOTE_FILE_NOT_FOUND = 78;

    /**
     * An unspecified error occurred during the SSH session.
     * @see CURLE_SSH (79)
     */
    const SSH = 79;

    /**
     * Failed to shut down the SSL connection.
     * @see CURLE_SSL_SHUTDOWN_FAILED (80)
     */
    const SSL_SHUTDOWN_FAILED = 80;

    /**
     * Socket is not ready for send/recv wait till it's ready and try again. This return code is only returned from
     * curl_easy_recv and curl_easy_send (Added in 7.18.2)
     * @see CURLE_AGAIN (81)
     */
    const AGAIN = 81;

    /**
     * Failed to load CRL file (Added in 7.19.0)
     * @see CURLE_SSL_CRL_BADFILE (82)
     */
    const SSL_CRL_BADFILE = 82;

    /**
     * Issuer check failed (Added in 7.19.0)
     * @see CURLE_SSL_ISSUER_ERROR (83)
     */
    const SSL_ISSUER_ERROR = 83;

    /**
     * The FTP server does not understand the PRET command at all or does not support the given argument.
     * Be careful when using CURLOPT_CUSTOMREQUEST, a custom LIST command will be sent with PRET CMD before PASV as well.
     * (Added in 7.20.0)
     * @see CURLE_FTP_PRET_FAILED (84)
     */
    const FTP_PRET_FAILED = 84;

    /**
     * Mismatch of RTSP CSeq numbers.
     * @see CURLE_RTSP_CSEQ_ERROR (85)
     */
    const RTSP_CSEQ_ERROR = 85;

    /**
     * Mismatch of RTSP Session Identifiers.
     * @see CURLE_RTSP_SESSION_ERROR (86)
     */
    const RTSP_SESSION_ERROR = 86;

    /**
     * Unable to parse FTP file list (during FTP wildcard downloading).
     * @see CURLE_FTP_BAD_FILE_LIST (87)
     */
    const FTP_BAD_FILE_LIST = 87;

    /**
     * Chunk callback reported error.
     * @see CURLE_CHUNK_FAILED (88)
     */
    const CHUNK_FAILED = 88;

    /**
     * (For internal use only, will never be returned by libcurl)
     * No connection available, the session will be queued. (added in 7.30.0)
     * @see CURLE_NO_CONNECTION_AVAILABLE (89)
     */
    const NO_CONNECTION_AVAILABLE = 89;

    /**
     * Failed to match the pinned key specified with CURLOPT_PINNEDPUBLICKEY.
     * @see CURLE_SSL_PINNEDPUBKEYNOTMATCH (90)
     */
    const SSL_PINNEDPUBKEYNOTMATCH = 90;

    /**
     * Status returned failure when asked with CURLOPT_SSL_VERIFYSTATUS.
     * @see CURLE_SSL_INVALIDCERTSTATUS (91)
     */
    const SSL_INVALIDCERTSTATUS = 91;

    /**
     * Stream error in the HTTP/2 framing layer.
     * @see CURLE_HTTP2_STREAM (92)
     */
    const HTTP2_STREAM = 92;

    /**
     * An API function was called from inside a callback.
     * @see CURLE_RECURSIVE_API_CALL (93)
     */
    const  RECURSIVE_API_CALL = 93;


    const MAP = [
        self::UNSUPPORTED_PROTOCOL      => 'CURLE_',
        self::FAILED_INIT               => 'CURLE_FAILED_INIT',
        self::URL_MALFORMAT             => 'CURLE_URL_MALFORMAT',
        self::NOT_BUILT_IN              => 'CURLE_NOT_BUILT_IN',
        self::COULDNT_RESOLVE_PROXY     => 'CURLE_COULDNT_RESOLVE_PROXY',
        self::COULDNT_RESOLVE_HOST      => 'CURLE_COULDNT_RESOLVE_HOST',
        self::COULDNT_CONNECT           => 'CURLE_COULDNT_CONNECT',
        self::FTP_WEIRD_SERVER_REPLY    => 'CURLE_FTP_WEIRD_SERVER_REPLY',
        self::REMOTE_ACCESS_DENIED      => 'CURLE_REMOTE_ACCESS_DENIED',
        self::FTP_ACCEPT_FAILED         => 'CURLE_FTP_ACCEPT_FAILED',
        self::FTP_WEIRD_PASS_REPLY      => 'CURLE_FTP_WEIRD_PASS_REPLY',
        self::FTP_ACCEPT_TIMEOUT        => 'CURLE_FTP_ACCEPT_TIMEOUT',
        self::FTP_WEIRD_PASV_REPLY      => 'CURLE_FTP_WEIRD_PASV_REPLY',
        self::FTP_WEIRD_227_FORMAT      => 'CURLE_FTP_WEIRD_227_FORMAT',
        self::FTP_CANT_GET_HOST         => 'CURLE_FTP_CANT_GET_HOST',
        self::HTTP2                     => 'CURLE_HTTP2',
        self::FTP_COULDNT_SET_TYPE      => 'CURLE_FTP_COULDNT_SET_TYPE',
        self::PARTIAL_FILE              => 'CURLE_PARTIAL_FILE',
        self::FTP_COULDNT_RETR_FILE     => 'CURLE_FTP_COULDNT_RETR_FILE',
        self::QUOTE_ERROR               => 'CURLE_QUOTE_ERROR',
        self::HTTP_RETURNED_ERROR       => 'CURLE_HTTP_RETURNED_ERROR',
        self::WRITE_ERROR               => 'CURLE_WRITE_ERROR',
        self::UPLOAD_FAILED             => 'CURLE_UPLOAD_FAILED',
        self::READ_ERROR                => 'CURLE_READ_ERROR',
        self::OUT_OF_MEMORY             => 'CURLE_OUT_OF_MEMORY',
        self::OPERATION_TIMEDOUT        => 'CURLE_OPERATION_TIMEDOUT',
        self::FTP_PORT_FAILED           => 'CURLE_FTP_PORT_FAILED',
        self::FTP_COULDNT_USE_REST      => 'CURLE_FTP_COULDNT_USE_REST',
        self::RANGE_ERROR               => 'CURLE_RANGE_ERROR',
        self::HTTP_POST_ERROR           => 'CURLE_HTTP_POST_ERROR',
        self::SSL_CONNECT_ERROR         => 'CURLE_SSL_CONNECT_ERROR',
        self::BAD_DOWNLOAD_RESUME       => 'CURLE_BAD_DOWNLOAD_RESUME',
        self::FILE_COULDNT_READ_FILE    => 'CURLE_FILE_COULDNT_READ_FILE',
        self::LDAP_CANNOT_BIND          => 'CURLE_LDAP_CANNOT_BIND',
        self::LDAP_SEARCH_FAILED        => 'CURLE_LDAP_SEARCH_FAILED',
        self::FUNCTION_NOT_FOUND        => 'CURLE_FUNCTION_NOT_FOUND',
        self::ABORTED_BY_CALLBACK       => 'CURLE_ABORTED_BY_CALLBACK',
        self::BAD_FUNCTION_ARGUMENT     => 'CURLE_BAD_FUNCTION_ARGUMENT',
        self::INTERFACE_FAILED          => 'CURLE_INTERFACE_FAILED',
        self::TOO_MANY_REDIRECTS        => 'CURLE_TOO_MANY_REDIRECTS',
        self::UNKNOWN_OPTION            => 'CURLE_UNKNOWN_OPTION',
        self::TELNET_OPTION_SYNTAX      => 'CURLE_TELNET_OPTION_SYNTAX',
        self::GOT_NOTHING               => 'CURLE_GOT_NOTHING',
        self::SSL_ENGINE_NOTFOUND       => 'CURLE_SSL_ENGINE_NOTFOUND',
        self::SSL_ENGINE_SETFAILED      => 'CURLE_SSL_ENGINE_SETFAILED',
        self::SEND_ERROR                => 'CURLE_SEND_ERROR',
        self::RECV_ERROR                => 'CURLE_RECV_ERROR',
        self::SSL_CERTPROBLEM           => 'CURLE_SSL_CERTPROBLEM',
        self::SSL_CIPHER                => 'CURLE_SSL_CIPHER',
        self::PEER_FAILED_VERIFICATION  => 'CURLE_PEER_FAILED_VERIFICATION',
        self::BAD_CONTENT_ENCODING      => 'CURLE_BAD_CONTENT_ENCODING',
        self::LDAP_INVALID_URL          => 'CURLE_LDAP_INVALID_URL',
        self::FILESIZE_EXCEEDED         => 'CURLE_FILESIZE_EXCEEDED',
        self::USE_SSL_FAILED            => 'CURLE_USE_SSL_FAILED',
        self::SEND_FAIL_REWIND          => 'CURLE_SEND_FAIL_REWIND',
        self::SSL_ENGINE_INITFAILED     => 'CURLE_SSL_ENGINE_INITFAILED',
        self::LOGIN_DENIED              => 'CURLE_LOGIN_DENIED',
        self::TFTP_NOTFOUND             => 'CURLE_TFTP_NOTFOUND',
        self::TFTP_PERM                 => 'CURLE_TFTP_PERM',
        self::REMOTE_DISK_FULL          => 'CURLE_REMOTE_DISK_FULL',
        self::TFTP_ILLEGAL              => 'CURLE_TFTP_ILLEGAL',
        self::TFTP_UNKNOWNID            => 'CURLE_TFTP_UNKNOWNID',
        self::REMOTE_FILE_EXISTS        => 'CURLE_REMOTE_FILE_EXISTS',
        self::TFTP_NOSUCHUSER           => 'CURLE_TFTP_NOSUCHUSER',
        self::CONV_FAILED               => 'CURLE_CONV_FAILED',
        self::CONV_REQD                 => 'CURLE_CONV_REQD',
        self::SSL_CACERT_BADFILE        => 'CURLE_SSL_CACERT_BADFILE',
        self::REMOTE_FILE_NOT_FOUND     => 'CURLE_REMOTE_FILE_NOT_FOUND',
        self::SSH                       => 'CURLE_SSH',
        self::SSL_SHUTDOWN_FAILED       => 'CURLE_SSL_SHUTDOWN_FAILED',
        self::AGAIN                     => 'CURLE_AGAIN',
        self::SSL_CRL_BADFILE           => 'CURLE_SSL_CRL_BADFILE',
        self::SSL_ISSUER_ERROR          => 'CURLE_SSL_ISSUER_ERROR',
        self::FTP_PRET_FAILED           => 'CURLE_FTP_PRET_FAILED',
        self::RTSP_CSEQ_ERROR           => 'CURLE_RTSP_CSEQ_ERROR',
        self::RTSP_SESSION_ERROR        => 'CURLE_RTSP_SESSION_ERROR',
        self::FTP_BAD_FILE_LIST         => 'CURLE_FTP_BAD_FILE_LIST',
        self::CHUNK_FAILED              => 'CURLE_CHUNK_FAILED',
        self::NO_CONNECTION_AVAILABLE   => 'CURLE_NO_CONNECTION_AVAILABLE',
        self::SSL_PINNEDPUBKEYNOTMATCH  => 'CURLE_SSL_PINNEDPUBKEYNOTMATCH',
        self::SSL_INVALIDCERTSTATUS     => 'CURLE_SSL_INVALIDCERTSTATUS',
        self::HTTP2_STREAM              => 'CURLE_HTTP2_STREAM',
        self::RECURSIVE_API_CALL        => 'CURLE_RECURSIVE_API_CALL',
    ];
}
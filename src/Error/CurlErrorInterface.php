<?php
namespace Keepper\Lib\Curl\Error;

interface CurlErrorInterface extends CurlErrorConstsInterface {

	/**
	 * Возвращает код ошибки
	 * @see https://curl.haxx.se/libcurl/c/libcurl-errors.html
	 * @return int
	 */
	public function code(): int;

	/**
	 * Получить текстовое описание для кода ошибки
	 * аналог функции curl_strerror()
	 * @return string|null
	 */
	public function messageByCode(): string;

	/**
	 * Возвращает сообщение об ошибке
	 * @return string
	 */
	public function message(): string;
}
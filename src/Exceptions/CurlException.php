<?php
namespace Keepper\Lib\Curl\Exceptions;

/**
 * Базовый класс для всех исключений библиотеки
 */
class CurlException extends \Exception {

}
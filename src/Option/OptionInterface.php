<?php
namespace Keepper\Lib\Curl\Option;

interface OptionInterface {

	/**
	 * TRUE для автоматической установки поля Referer: в запросах, перенаправленных заголовком Location:.
	 * @see CURLOPT_AUTOREFERER
	 */
	const AUTOREFERER = 58;

	/**
	 * TRUE для возврата необработанного ответа при использовании константы @see CURLOPT_RETURNTRANSFER
	 * @see CURLOPT_BINARYTRANSFER
	 */
	const BINARYTRANSFER = 19914;

	/**
	 * TRUE для указания текущему сеансу начать новую "сессию" cookies. Это заставит libcurl проигнорировать все "сессионные" cookies, которые она должна была бы загрузить, полученные из предыдущей сессии. По умолчанию libcurl всегда сохраняет и загружает все cookies, вне зависимости от того, являются ли они "сессионными" или нет. "Сессионные" cookies - это cookies без срока истечения, которые должны существовать только для текущей "сессии".
	 * @see CURLOPT_COOKIESESSION
	 */
	const COOKIESESSION = 96;

	/**
	 * TRUE для вывода информации о сертификате SSL в поток STDERR при безопасных соединениях.
	 * @see CURLOPT_CERTINFO
	 */
	const CERTINFO = 172;

	/**
	 * TRUE сообщает библиотеке, чтобы она провела необходимые аутентификацию прокси и настройку соединения, но не передавала данные. Эта опция реализована для HTTP, SMTP и POP3.
	 * @see CURLOPT_CONNECT_ONLY
	 */
	const CONNECT_ONLY = 141;

	/**
	 * TRUE для преобразования концов строк Unix в CRLF.
	 * @see CURLOPT_CRLF
	 */
	const CRLF = 27;

	/**
	 * TRUE для использования глобального кеша DNS. Этот параметр не является потокобезопасным и по умолчанию включен.
	 * @see CURLOPT_DNS_USE_GLOBAL_CACHE
	 */
	const DNS_USE_GLOBAL_CACHE = 91;

	/**
	 * TRUE для подробного отчета при неудаче, если полученный HTTP-код больше или равен 400. Поведение по умолчанию возвращает страницу как обычно, игнорируя код.
	 * @see CURLOPT_FAILONERROR
	 */
	const FAILONERROR = 45;

	/**
	 * TRUE для разрешения ложного старта TLS.
	 * @see CURLOPT_SSL_FALSESTART
	 */
	const SSL_FALSESTART = 233;

	/**
	 * TRUE для попытки получения даты модификации удаленного документа.
	 * Это значение может быть получено с помощью параметра @see CURLINFO_FILETIME из функции @see curl_getinfo().
	 * @see CURLOPT_FILETIME
	 */
	const FILETIME = 69;

	/**
	 * TRUE для следования любому заголовку "Location: ", отправленному сервером в своем ответе (учтите, что это происходит рекурсивно, PHP будет следовать за всеми посылаемыми заголовками "Location: ", за исключением случая, когда установлена константа @see CURLOPT_MAXREDIRS
	 * @see CURLOPT_FOLLOWLOCATION
	 */
	const FOLLOWLOCATION = 52;

	/**
	 * TRUE для принудительного закрытия соединения после завершения его обработки так, чтобы его нельзя было использовать повторно.
	 * @see CURLOPT_FORBID_REUSE
	 */
	const FORBID_REUSE = 75;

	/**
	 * TRUE для принудительного использования нового соединения вместо закешированного.
	 * @see CURLOPT_FRESH_CONNECT
	 */
	const FRESH_CONNECT = 74;

	/**
	 * TRUE для использования EPRT (и LPRT) при активных FTP-загрузках. Используйте FALSE для того, чтобы отключить EPRT и LPRT и использовать только PORT.
	 * @see CURLOPT_FTP_USE_EPRT
	 */
	const FTP_USE_EPRT = 106;

	/**
	 * TRUE для первоначальной пробы команды EPSV при FTP-передачах. Если команда не удалась, будет произведен обратный откат к PASV. Установите в FALSE для отключения EPSV.
	 * @see CURLOPT_FTP_USE_EPSV
	 */
	const FTP_USE_EPSV = 85;

	/**
	 * TRUE для создания отсутствующих директорий, если FTP-операция обнаруживает несуществующий путь.
	 * @see CURLOPT_FTP_CREATE_MISSING_DIRS
	 */
	const FTP_CREATE_MISSING_DIRS = 110;

	/**
	 * TRUE для записи в конец удаленного файла, вместо перезаписывания.
	 * @see CURLOPT_FTPAPPEND
	 */
	const FTP_APPEND = 50;

	/**
	 * TRUE для отключения алгоритма Нейгла, который пытается уменьшить количество мелких пакетов в сети.
	 * @see CURLOPT_TCP_NODELAY
	 */
	const TCP_NODELAY = 121;

	/**
	 * TRUE для возврата только списка имен из FTP-директории.
	 * @see CURLOPT_FTPLISTONLY
	 */
	const FTPLISTONLY = 48;

	/**
	 * TRUE для включения заголовков в вывод.
	 * @see CURLOPT_HEADER
	 */
	const HEADER = 42;

	/**
	 * TRUE для отслеживания строки запроса дескриптора. Префикс CURLINFO_ используется специально.
	 * @see CURLINFO_HEADER_OUT
	 */
	const HEADER_OUT = 2;

	/**
	 * TRUE для сброса метода HTTP-запроса на метод GET. Так как GET используется по умолчанию, этот параметр необходим только в случае, если метод запроса был ранее изменен.
	 * @see CURLOPT_HTTPGET
	 */
	const HTTPGET = 80;

	/**
	 * TRUE для туннелирования через указанный HTTP-прокси.
	 * @see CURLOPT_HTTPPROXYTUNNEL
	 */
	const HTTPPROXYTUNNEL = 61;

	/**
	 * TRUE для полного отключения сообщений функций cURL.
	 * @see CURLOPT_MUTE
	 */
	const MUTE = -1;

	/**
	 * TRUE для считывания файла ~/.netrc, чтобы найти логин и пароль для удаленного сайта, с которым устанавливается соединение.
	 * @see CURLOPT_NETRC
	 */
	const NETRC = 51;

	/**
	 * TRUE для исключения тела ответа из вывода. Метод запроса устанавливается в HEAD. Смена этого параметра в FALSE не меняет его обратно в GET.
	 * @see CURLOPT_NOBODY
	 */
	const NOBODY = 44;

	/**
	 * TRUE для отмены индикатора прогресса при передачах cURL.
	 * @see CURLOPT_NOPROGRESS
	 */
	const NOPROGRESS = 43;

	/**
	 * TRUE для игнорирования любой функции cURL, посылающей сигналы PHP процессу. Этот параметр включен по умолчанию в многопоточных SAPI для корректной работы параметров тайм-аута.
	 * @see CURLOPT_NOSIGNAL
	 */
	const NOSIGNAL = 99;

	/**
	 * TRUE для игнорирования последовательностей с двумя точками.
	 * @see CURLOPT_PATH_AS_IS
	 */
	const PATH_AS_IS = 234;

	/**
	 * TRUE для ожидания конвеера/мультиплексирования.
	 * @see CURLOPT_PIPEWAIT
	 */
	const PIPEWAIT = 237;

	/**
	 * RUE для использования обычного HTTP POST. Данный метод POST использует обычный application/x-www-form-urlencoded, обычно используемый в HTML-формах.
	 * @see CURLOPT_POST
	 */
	const POST = 47;

	/**
	 * TRUE для загрузки файла методом HTTP PUT.
	 * Используемый файл должен быть установлен с помощью параметров @see CURLOPT_INFILE и @see CURLOPT_INFILESIZE.
	 * @see CURLOPT_PUT
	 */
	const PUT = 54;

	/**
	 * TRUE для возврата результата передачи в качестве строки из curl_exec() вместо прямого вывода в браузер.
	 * @see CURLOPT_RETURNTRANSFER
	 */
	const RETURNTRANSFER = 19913;

	/**
	 * TRUE для отключения поддержки префикса @ для загружаемых файлов в CURLOPT_POSTFIELDS, который означает, что значения, переданные с @ могут безопасно передаваться в виде полей. Вместо префикса можно использовать опцию CURLFile.
	 * @see CURLOPT_SAFE_UPLOAD
	 */
	const SAFE_UPLOAD = -1;

	/**
	 * TRUE для разрешения отправки инициирующего ответа в первом пакете.
	 * @see CURLOPT_SASL_IR
	 */
	const SASL_IR = 218;

	/**
	 * FALSE для отключения ALPN в SSL handshake (если на бекенде SSL libcurl собран с ее поддержкой), это можно использовать при соединении http2.
	 * @see CURLOPT_SSL_ENABLE_ALPN
	 */
	const SSL_ENABLE_ALPN = 226;

	/**
	 * FALSE для отключения ALPN в SSL handshake (если на бэкенде SSL libcurl собран с ее поддержкой), это можно использовать при соединении http2.
	 * @see CURLOPT_SSL_ENABLE_NPN
	 */
	const SSL_ENABLE_NPN = 225;

	/**
	 * FALSE для остановки cURL от проверки сертификата узла сети.
	 * Альтернативные сертификаты дл проверки могут быть указаны с помощью параметра @see CURLOPT_CAINFO, или директории с сертификатами, указываемой параметром @see CURLOPT_CAPATH.
	 * @see CURLOPT_SSL_VERIFYPEER
	 */
	const SSL_VERIFYPEER = 64;

	/**
	 * TRUE для проверки статуса сертификата.
	 * @see CURLOPT_SSL_VERIFYSTATUS
	 */
	const SSL_VERIFYSTATUS = 232;

	/**
	 * TRUE для разрешения TCP Fast Open.
	 * @see CURLOPT_TCP_FASTOPEN
	 */
	const TCP_FASTOPEN = 244;

	/**
	 * TRUE для запрета посылки запроса TFTP опций.
	 * @see CURLOPT_TFTP_NO_OPTIONS
	 */
	const TFTP_NO_OPTIONS = 242;

	/**
	 * TRUE для использования ASCII-режима при FTP-передачах.
	 * При использовании LDAP данные возвращаются простым текстом вместо HTML.
	 * В Windows системах поток STDOUT не устанавливается в бинарный режим.
	 * @see CURLOPT_TRANSFERTEXT
	 */
	const TRANSFERTEXT = 53;

	/**
	 * TRUE для продолжения отправки логина и пароля при редиректах (используя @see CURLOPT_FOLLOWLOCATION), даже при изменении имени хоста.
	 * @see CURLOPT_UNRESTRICTED_AUTH
	 */
	const UNRESTRICTED_AUTH = 105;

	/**
	 * TRUE для подготовки к загрузке файла на сервер.
	 * @see CURLOPT_UPLOAD
	 */
	const UPLOAD = 46;

	/**
	 * TRUE для вывода дополнительной информации.
	 * Записывает вывод в поток STDERR, или файл, указанный параметром CURLOPT_STDERR.
	 * @see CURLOPT_VERBOSE
	 */
	const VERBOSE = 41;

	/**
	 * Размер буфера, используемого при каждом чтении. Однако, нет никакой гарантии что данный запрос будет завершен.
	 * @see CURLOPT_BUFFERSIZE
	 */
	const BUFFERSIZE = 98;

	/**
	 * Количество секунд ожидания при попытке соединения. Используйте 0 для бесконечного ожидания.
	 * @see CURLOPT_CONNECTTIMEOUT
	 */
	const CONNECTTIMEOUT = 78;

	/**
	 * Количество миллисекунд ожидания при попытке соединения.
	 * Используйте 0 для бесконечного ожидания.
	 * Если библиотека libcurl скомпилирована с использованием стандартного системного преобразователя имен,
	 * то соединение будет по-прежнему использовать полносекундное ожидание в качестве тайм-аута с минимально
	 * допустимым тайм-аутом в 1 секунду.
	 * @see CURLOPT_CONNECTTIMEOUT_MS
	 */
	const CONNECTTIMEOUT_MS = 156;

	/**
	 * Количество секунд, в течение которых в памяти хранятся DNS-записи. По умолчанию этот параметр равен 120 (2 минуты).
	 * @see CURLOPT_DNS_CACHE_TIMEOUT
	 */
	const DNS_CACHE_TIMEOUT = 92;

	/**
	 * Таймаут для ответов с заголовком Expect: 100-continue в миллисекундах. По умолчанию - 1000 миллисекунд.
	 * @see CURLOPT_EXPECT_100_TIMEOUT_MS
	 */
	const EXPECT_100_TIMEOUT_MS = 227;

	/**
	 * Метод FTP-аутентификации (в активном режиме): CURLFTPAUTH_SSL (сначала проверяется SSL),
	 * CURLFTPAUTH_TLS (сначала проверяется TLS) или CURLFTPAUTH_DEFAULT (cURL решает сама).
	 * @see CURLOPT_FTPSSLAUTH
	 */
	const FTPSSLAUTH = 129;

	/**
	 * Что делать с заголовками. Одна из следующих опций: CURLHEADER_UNIFIED: заголовки, указанные в
	 * CURLOPT_HTTPHEADER будут использованы в запросах к серверам и прокси. Если разрешена эта опция,
	 * CURLOPT_PROXYHEADER не будет иметь эффекта. CURLHEADER_SEPARATE: заголовки CURLOPT_HTTPHEADER будут
	 * отсылаться только на сервер, но не на прокси. На прокси заголовки должны посылаться CURLOPT_PROXYHEADER.
	 * Обратите внимание, что если запросы типа non-CONNECT будут слаться на прокси, то libcurl будет посылать
	 * как серверные, так и прокси заголовки. Если же происходит CONNECT, libcurl пошлет на прокси только
	 * заголовки CURLOPT_PROXYHEADER, а потом заголовки CURLOPT_HTTPHEADER только на сервер. По умолчанию
	 * CURLHEADER_SEPARATE, начиная с cURL 7.42.1, и CURLHEADER_UNIFIED до него.
	 * @see CURLOPT_HEADEROPT
	 */
	const HEADEROPT = 229;

	/**
	 * CURL_HTTP_VERSION_NONE (по умолчанию CURL сам выбирает используемую версию),
	 * @see CURL_HTTP_VERSION_1_0 (принудительное использование HTTP/1.0),
	 * или @see CURL_HTTP_VERSION_1_1 (принудительное использование HTTP/1.1).
	 * @see CURLOPT_HTTP_VERSION
	 */
	const HTTP_VERSION = 84;

	/**
	 * Используемые HTTP-методы авторизации. Используемые параметры:
	 * @see CURLAUTH_BASIC,
	 * @see CURLAUTH_DIGEST,
	 * @see CURLAUTH_GSSNEGOTIATE,
	 * @see CURLAUTH_NTLM,
	 * @see CURLAUTH_ANY и
	 * @see CURLAUTH_ANYSAFE.
	 * Можно использовать побитовый оператор | (или) для комбинации нескольких методов вместе. В этом случае cURL
	 * опросит сервер на предмет поддерживаемых методов авторизации и выберет лучший из них.
	 * @see CURLAUTH_ANY - это псевдоним @see CURLAUTH_BASIC | @see CURLAUTH_DIGEST | @see CURLAUTH_GSSNEGOTIATE | @see CURLAUTH_NTLM.
	 * @see CURLAUTH_ANYSAFE - это псевдоним @see CURLAUTH_DIGEST | @see CURLAUTH_GSSNEGOTIATE | @see CURLAUTH_NTLM.
	 * @see CURLOPT_HTTPAUTH
	 */
	const HTTPAUTH = 107;

	/**
	 * @see CURLAUTH_ANY
	 */
	const AUTH_ANY = -17;

	/**
	 * @see CURLAUTH_ANYSAFE
	 */
	const AUTH_ANYSAFE = -18;

	/**
	 * Ожидаемый размер файла в байтах при загрузке файла на удаленный сервер. Учтите, что использование этой опции не
	 * остановит дальнейшую посылку данных, превышающих это значение, так как посылаемые данные зависят
	 * от результата @see CURLOPT_READFUNCTION.
	 * @see CURLOPT_INFILESIZE
	 */
	const INFILESIZE = 14;

	/**
	 * Верхний порог скорости передачи данных, в байтах в секунду.
	 * Проверка происходит в течение @see CURLOPT_LOW_SPEED_TIME секунд, после чего PHP считает передачу слишком
	 * медленной и прерывает ее.
	 * @see CURLOPT_LOW_SPEED_LIMIT
	 */
	const LOW_SPEED_LIMIT = 19;

	/**
	 * Максимальное количество секунд, в течение которых скорость передачи не должна превышать @see CURLOPT_LOW_SPEED_LIMIT,
	 * иначе PHP пометит передачу как слишком медленную и прекратит ее.
	 * @see CURLOPT_LOW_SPEED_TIME
	 */
	const LOW_SPEED_TIME = 20;

	/**
	 * Максимальное количество постоянных соединений. При достижении лимита для определения закрываемого
	 * соединения используется параметр @see CURLOPT_CLOSEPOLICY.
	 * @see CURLOPT_MAXCONNECTS
	 */
	const MAXCONNECTS = 71;

	/**
	 * Максимальное количество принимаемых редиректов. Используйте этот параметр вместе
	 * с параметром @see CURLOPT_FOLLOWLOCATION.
	 * @see CURLOPT_MAXREDIRS
	 */
	const MAXREDIRS = 68;

	/**
	 * Альтернативный порт соединения.
	 * @see CURLOPT_PORT
	 */
	const PORT = 3;

	/**
	 * Битовая маска, содержащая 1 (301 Moved Permanently), 2 (302 Found) и 4 (303 See Other), чтобы задавать
	 * должен ли метод HTTP POST обрабатываться при включенной опции @see CURLOPT_FOLLOWLOCATION,
	 * если произошел указанный тип перенаправления.
	 * @see CURLOPT_POSTREDIR
	 */
	const POSTREDIR = 161;

	/**
	 * Битовая маска из значений CURLPROTO_
	 * Данная маска ограничивает используемые libcurl протоколы. Это позволяет иметь libcurl, работающую с
	 * большим количеством протоколов, и ограничивать работу определенных передач только для некоторого их набора.
	 * По умолчанию libcurl использует все поддерживаемые протоколы. Смотрите также параметр @see CURLOPT_REDIR_PROTOCOLS.
	 * Корректные значения протоколов:
	 * @see CURLPROTO_HTTP,
	 * @see CURLPROTO_HTTPS,
	 * @see CURLPROTO_FTP,
	 * @see CURLPROTO_FTPS,
	 * @see CURLPROTO_SCP,
	 * @see CURLPROTO_SFTP,
	 * @see CURLPROTO_TELNET,
	 * @see CURLPROTO_LDAP,
	 * @see CURLPROTO_LDAPS,
	 * @see CURLPROTO_DICT,
	 * @see CURLPROTO_FILE,
	 * @see CURLPROTO_TFTP,
	 * @see CURLPROTO_ALL
	 * @see CURLOPT_PROTOCOLS
	 */
	const PROTOCOLS = 181;

	/**
	 * Методы авторизации HTTP, используемые при соединении с прокси-сервером. Используйте те же самые битовые маски,
	 * которые были описаны у параметра @see CURLOPT_HTTPAUTH. В данный момент для авторизации прокси
	 * поддерживаются только @see CURLAUTH_BASIC и @see CURLAUTH_NTLM.
	 * @see CURLOPT_PROXYAUTH
	 */
	const PROXYAUTH = 111;

	/**
	 * Номер порта прокси-сервера, к которому осуществляется соединение. Этот номер также может быть
	 * установлен с помощью параметра @see CURLOPT_PROXY.
	 * @see CURLOPT_PROXYPORT
	 */
	const PROXYPORT = 59;

	/**
	 * Либо @see CURLPROXY_HTTP (по умолчанию), либо
	 * @see CURLPROXY_SOCKS4, @see CURLPROXY_SOCKS5, @see CURLPROXY_SOCKS4A или @see CURLPROXY_SOCKS5_HOSTNAME.
	 * @see CURLOPT_PROXYTYPE
	 */
	const PROXYTYPE = 101;

	/**
	 * Битовая маска из значений CURLPROTO_*. Данная битовая масска ограничивает протоколы, используемые libcurl
	 * при редиректе (при включенном параметре CURLOPT_FOLLOWLOCATION). Это позволяет ограничить набор используемых
	 * протоколов при редиректах для некоторых передач. По умолчанию, libcurl поддерживает все протоколы,
	 * кроме FILE и SCP. В версиях, предшествовавших 7.19.4, перенаправление использовалось для всех протоколов без
	 * исключения. Смотрите также описание параметра @see CURLOPT_PROTOCOLS для списка констант со значениями протоколов.
	 * @see CURLOPT_REDIR_PROTOCOLS
	 */
	const REDIR_PROTOCOLS = 182;

	/**
	 * Смещение в байтах для возобновления передачи.
	 * @see CURLOPT_RESUME_FROM
	 */
	const RESUME_FROM = 21;

	/**
	 * Устанавливает опции поведения SSL. Битовая маска из следующих констант:
	 * @see CURLSSLOPT_ALLOW_BEAST: не пытаться найти обходной путь для изъяна в безопасности протоколов SSL3 и TLS1.0.
	 * @see CURLSSLOPT_NO_REVOKE: не производить проверку, отозван ли сертификат, для тех SSL бэкендов, где оно есть.
	 * @see CURLOPT_SSL_OPTIONS
	 */
	const SSL_OPTIONS = 216;

	/**
	 * Используйте 1 для проверки существования общего имени в сертификате SSL. Используйте 2 для проверки существования
	 * общего имени и также его совпадения с указанным хостом. 0 чтобы не проверять имена. В боевом окружении значение
	 * этого параметра должно быть 2 (установлено по умолчанию).
	 * @see CURLOPT_SSL_VERIFYHOST
	 */
	const SSL_VERIFYHOST = 81;

	/**
	 * Одна из констант
	 * @see CURL_SSLVERSION_DEFAULT (0),
	 * @see CURL_SSLVERSION_TLSv1 (1),
	 * @see CURL_SSLVERSION_SSLv2 (2),
	 * @see CURL_SSLVERSION_SSLv3 (3),
	 * @see CURL_SSLVERSION_TLSv1_0 (4),
	 * @see CURL_SSLVERSION_TLSv1_1 (5) или
	 * @see CURL_SSLVERSION_TLSv1_2 (6).
	 *
	 * Рекомендуется не устанавливать эту опцию и оставить значение по умолчанию.
	 * Установка в 2 или 3 опасно и допускает применение известных уязвимостей в SSLv2 и SSLv3.
	 *
	 * @see CURLOPT_SSLVERSION
	 */
	const SSLVERSION = 32;

	/**
	 * Установить вес цифрового потока (число между 1 и 256).
	 * @see CURLOPT_STREAM_WEIGHT
	 */
	const STREAM_WEIGHT = 239;

	/**
	 * Способ трактовки параметра @see CURLOPT_TIMEVALUE. Используйте @see CURL_TIMECOND_IFMODSINCE для возвращения
	 * страницы, только если она была изменена со времени, указанного в параметре @see CURLOPT_TIMEVALUE.
	 * Если страница не была изменена, вернется заголовок "304 Not Modified", подразумевая, что
	 * параметр @see CURLOPT_HEADER установлен в TRUE. Используйте @see CURL_TIMECOND_IFUNMODSINCE для обратного эффекта.
	 * По умолчанию используется @see CURL_TIMECOND_IFMODSINCE.
	 *
	 * @see CURLOPT_TIMECONDITION
	 */
	const TIMECONDITION = 33;

	/**
	 * Максимально позволенное количество секунд для выполнения cURL-функций.
	 * @see CURLOPT_TIMEOUT
	 */
	const TIMEOUT = 13;

	/**
	 * Максимально позволенное количество миллисекунд для выполнения cURL-функций. Если libcurl собрана с
	 * использованием обычного системного распознавателя имен, то этот промежуток соединения все еще будет
	 * использовать секундное округление тайм-аутов, с минимально разрешенным тайм-аутом в одну секунду.
	 * @see CURLOPT_TIMEOUT_MS
	 */
	const TIMEOUT_MS = 155;

	/**
	 * Количество секунд, начиная с 1 января 1970 года. Это время будет использовано
	 * параметром @see CURLOPT_TIMECONDITION. По умолчанию,
	 * используется параметр @see CURL_TIMECOND_IFMODSINCE.
	 * @see CURLOPT_TIMEVALUE
	 */
	const TIMEVALUE = 34;

	/**
	 * Если скорость скачки превысит это значение (указанное в байтах в секунду) в среднем в течение всей передачи,
	 * то скачка будет приостановлена для поддержания средней скорости меньше либо равной данному параметру.
	 * По умолчанию скорость не ограничивается.
	 * @see CURLOPT_MAX_RECV_SPEED_LARGE
	 */
	const MAX_RECV_SPEED_LARGE = 30146;

	/**
	 * Если загрузка на сервер превысит это значение (указанное в байтах в секунду) в среднем в течение всей передачи,
	 * то загрузка будет приостановлена для поддержания средней скорости меньше либо равной данному параметру.
	 * По умолчанию скорость не ограничивается.
	 * @see CURLOPT_MAX_SEND_SPEED_LARGE
	 */
	const MAX_SEND_SPEED_LARGE = 30145;

	/**
	 * Битовая маска, состоящая из одной или более констант:
	 * @see CURLSSH_AUTH_PUBLICKEY,
	 * @see CURLSSH_AUTH_PASSWORD,
	 * @see CURLSSH_AUTH_HOST,
	 * @see CURLSSH_AUTH_KEYBOARD. Установите @see CURLSSH_AUTH_ANY, чтобы libcurl сам выбрал одну из них.
	 *
	 * @see CURLOPT_SSH_AUTH_TYPES
	 */
	const SSH_AUTH_TYPES = 151;

	/**
	 * Позволяет приложению выбрать вид IP-адреса, с которым определяется имя хоста. Это необходимо, если
	 * используется имя хоста, которое получается с использованием более одной версии IP адреса. Возможными
	 * значениями могут быть @see CURL_IPRESOLVE_WHATEVER, @see CURL_IPRESOLVE_V4, @see CURL_IPRESOLVE_V6,
	 * и по умолчанию @see CURL_IPRESOLVE_WHATEVER.
	 * @see CURLOPT_IPRESOLVE
	 */
	const IPRESOLVE = 113;

	/**
	 * Какой метод использовать, чтобы достать файл на сервере FTP.
	 * Варианты: @see CURLFTPMETHOD_MULTICWD, @see CURLFTPMETHOD_NOCWD и @see CURLFTPMETHOD_SINGLECWD.
	 * @see CURLOPT_FTP_FILEMETHOD
	 */
	const FTP_FILEMETHOD = 138;

	/**
	 * Имя файла, содержащего один или более сертификатов, с которыми будут сверяться узлы.
	 * Этот параметр имеет смысл только при использовании совместно с @see CURLOPT_SSL_VERIFYPEER.
	 * @see CURLOPT_CAINFO
	 */
	const CAINFO = 10065;

	/**
	 * Директория, содержащая несколько CA сертификатов.
	 * Используйте этот параметр совместно с @see CURLOPT_SSL_VERIFYPEER.
	 * @see CURLOPT_CAPATH
	 */
	const CAPATH = 10097;

	/**
	 * Содержимое заголовка "Cookie: ", используемого в HTTP-запросе. Обратите внимание, что несколько cookies
	 * разделяются точкой с запятой с последующим пробелом (например, "fruit=apple; colour=red")
	 * @see CURLOPT_COOKIE
	 */
	const COOKIE = 10022;

	/**
	 * Имя файла, содержащего cookies. Данный файл должен быть в формате Netscape или просто заголовками HTTP,
	 * записанными в файл. Если в качестве имени файла передана пустая строка, то cookies сохраняться не будут,
	 * но их обработка все еще будет включена.
	 * @see CURLOPT_COOKIEFILE
	 */
	const COOKIEFILE = 10031;

	/**
	 * Имя файла, в котором будут сохранены все внутренние cookies текущей передачи после закрытия дескриптора,
	 * например, после вызова curl_close.
	 * @see CURLOPT_COOKIEJAR
	 */
	const COOKIEJAR = 10082;

	/**
	 * Собственный метод запроса, используемый вместо "GET" или "HEAD" при выполнении HTTP-запроса. Это полезно
	 * при запросах "DELETE" или других, более редких HTTP-запросах. Корректными значениями будут такие
	 * как "GET", "POST", "CONNECT" и так далее; т.е. не вводите здесь всю строку с HTTP-запросом.
	 * Например, указание "GET /index.html HTTP/1.0\r\n\r\n" будет неправильным.
	 * @see CURLOPT_CUSTOMREQUEST
	 */
	const CUSTOMREQUEST = 10036;

	/**
	 * Протокол по умолчанию, если он отсутствует в схеме URL.
	 * @see CURLOPT_DEFAULT_PROTOCOL
	 */
	const DEFAULT_PROTOCOL = 10238;

	/**
	 * Устанавливает имя сетевого интерфейса, к которому привязан DNS. Это должно быть имя интерфейса, а не адрес.
	 * @see CURLOPT_DNS_INTERFACE
	 */
	const DNS_INTERFACE = 10221;

	/**
	 * Установить локальный IPv4-адрес, по которому доступне DNS. Адрес должен быть представлен в виде строки,
	 * содержащий одно число.
	 * @see CURLOPT_DNS_LOCAL_IP4
	 */
	const DNS_LOCAL_IP4 = 10222;

	/**
	 * Установить локальный IPv6-адрес, по которому доступен DNS. Адрес должен быть представлен в виде
	 * строки, содержащий одно число.
	 * @see CURLOPT_DNS_LOCAL_IP6
	 */
	const DNS_LOCAL_IP6 = 10223;

	/**
	 * Наподобие @see CURLOPT_RANDOM_FILE, за исключением того, что имя файла устанавливается в сокет Entropy Gathering Daemon.
	 * @see CURLOPT_EGDSOCKET
	 */
	const EGDSOCKET = 10077;

	/**
	 * Содержимое заголовка "Accept-Encoding: ". Это позволяет декодировать запрос. Поддерживаемыми кодировками
	 * являются "identity", "deflate" и "gzip". Если передана пустая строка, "", посылается заголовок,
	 * содержащий все поддерживаемые типы кодировок.
	 * @see CURLOPT_ENCODING
	 */
	const ENCODING = 10102;

	/**
	 * Значение, которое будет использоваться для определения IP-адреса для команды "PORT" FTP-протокола.
	 * Команда "PORT" сообщает серверу, с каким IP-адресом он должен устанавливать соединение.
	 * Это может быть IP-адрес, имя хоста, имя сетевого интерфейса (под Unix) или просто '-' для использования с
	 * истемного IP-адреса по умолчанию.
	 * @see CURLOPT_FTPPORT
	 */
	const FTPPORT = 10017;

	/**
	 * Имя используемого сетевого интерфейса. Может быть именем интерфейса, IP адресом или именем хоста.
	 * @see CURLOPT_INTERFACE
	 */
	const INTERFACE = 10062;

	/**
	 * Пароль, который требуется для использования закрытого ключа @see CURLOPT_SSLKEY или @see CURLOPT_SSH_PRIVATE_KEYFILE.
	 * @see CURLOPT_KEYPASSWD
	 */
	const KEYPASSWD = 10026;

	/**
	 * Уровень безопасности KRB4 (Kerberos 4). Любое из следующих значений (в порядке от слабого к самому сильному)
	 * корректно: "clear", "safe", "confidential", "private".. Если указанная строка отличается от данных значений,
	 * будет использовано значение "private". Установка этого параметра в NULL полностью отключит безопасность KRB4.
	 * На данный момент безопасность KRB4 работает только с FTP-транзакциями.
	 * @see CURLOPT_KRB4LEVEL
	 */
	const KRB4LEVEL = 10063;

	/**
	 * Используется для установки специфичных для протокола настроек логина, таких как предпочитаемый механизм
	 * аутентификации "AUTH=NTLM" или "AUTH=*", и должна использоваться совместно с @see CURLOPT_USERNAME.
	 * @see CURLOPT_LOGIN_OPTIONS
	 */
	const LOGIN_OPTIONS = 10224;

	/**
	 * Устанавливает зафиксированный открытый ключ. Строка должны содержать имя файла, в котором лежит ваш
	 * зафиксированный открытый ключ. Ожидается формат файла "PEM" или "DEX". Строка также может быть числом
	 * в формате base64, закодированным sha256 с префиксом "sha256//" и разделенная точкой с запятой ";".
	 * @see CURLOPT_PINNEDPUBLICKEY
	 */
	const PINNEDPUBLICKEY = 10230;

	/**
	 * Все данные, передаваемые в HTTP POST-запросе. Для передачи файла, укажите перед именем файла @, а также
	 * используйте полный путь к файлу. Тип файла также может быть указан с помощью формата ';type=mimetype',
	 * следующим за именем файла. Этот параметр может быть передан как в качестве url-закодированной строки,
	 * наподобие 'para1=val1&para2=val2&...', так и в виде массива, ключами которого будут имена полей,
	 * а значениями - их содержимое. Если value является массивом, заголовок Content-Type будет установлен
	 * в значение multipart/form-data. Начиная с PHP 5.2.0, при передаче файлов с префиксом @, value должен быть массивом.
	 * С PHP 5.5.0 префикс @ устарел и файлы можно отправлять с помощью CURLFile. Префикс @ можно отключить,
	 * чтобы можно было передавать значения, начинающиеся с @, задав опцию @see CURLOPT_SAFE_UPLOAD в значение TRUE.
	 * @see CURLOPT_POSTFIELDS
	 */
	const POSTFIELDS = 10015;

	/**
	 * Любые данные, которые должны связаны с этим дескриптором cURL. Эти данные могут быть выбраны подзапросом
	 * опции @see CURLINFO_PRIVATE функции curl_getinfo(). cURL ничего не делает с этими данными.
	 * Если используется множество дескрипторов cURL, эти данные обычно используются как уникальный ключ для
	 * определения дескриптора cURL
	 * @see CURLOPT_PRIVATE
	 */
	const PRIVATE = 10103;

	/**
	 * HTTP-прокси, через который будут направляться запросы.
	 * @see CURLOPT_PROXY
	 */
	const PROXY = 10004;

	/**
	 * Имя сервиса аутентификации прокси.
	 * @see CURLOPT_PROXY_SERVICE_NAME
	 */
	const PROXY_SERVICE_NAME = 10235;

	/**
	 * Логин и пароль, записанные в виде "[username]:[password]", используемые при соединении через прокси.
	 * @see CURLOPT_PROXYUSERPWD
	 */
	const PROXYUSERPWD = 10006;

	/**
	 * Имя файла, используемого для инициализации генератора случайных чисел для SSL.
	 * @see CURLOPT_RANDOM_FILE
	 */
	const RANDOM_FILE = 10076;

	/**
	 * Диапазон данных, которые нужно загрузить, в формате "X-Y", причем либо X, либо Y могут быть опущены.
	 * Протокол HTTP также поддерживает передачу нескольких диапазонов, разделенных запятыми, они задаются в формате "X-Y,N-M".
	 * @see CURLOPT_RANGE
	 */
	const RANGE = 10007;

	/**
	 * Содержимое заголовка "Referer: ", который будет использован в HTTP-запросе.
	 * @see CURLOPT_REFERER
	 */
	const REFERER = 10016;

	/**
	 * Имя сервиса аутентификации.
	 * @see CURLOPT_SERVICE_NAME
	 */
	const SERVICE_NAME = 10236;

	/**
	 * Строка, содержащая 32 шестнадцатеричных цифры. Строка должна являться контрольной суммой
	 * по алгоритму MD5 открытого ключа удаленного компьютера и libcurl будет сбрасывать соединение к удаленному хосту
	 * до тех пор, пока контрольная сумма не будет соответствовать публичному ключу. Эта опция предназначена только
	 * для передачи данных с помощью SCP и SFTP.
	 * @see CURLOPT_SSH_HOST_PUBLIC_KEY_MD5
	 */
	const SSH_HOST_PUBLIC_KEY_MD5 = 10162;

	/**
	 * Имя файла для вашего публичного ключа. Если не задано, то libcurl использует по умолчанию файл $HOME/.ssh/id_dsa.pub,
	 * если переменная окружения HOME установлена и файл "id_dsa.pub" в текущей директории, если переменная HOME не установлена.
	 * @see CURLOPT_SSH_PUBLIC_KEYFILE
	 */
	const SSH_PUBLIC_KEYFILE = 10152;

	/**
	 * Имя файла для вашего приватного ключа. Если не задано, то libcurl использует по умолчанию файл $HOME/.ssh/id_dsa,
	 * если переменная окружения HOME установлена и файл "id_dsa" в текущей директории, если переменная HOME не установлена.
	 * Если файл защищен паролем, установите пароль с помощью @see CURLOPT_KEYPASSWD.
	 * @see CURLOPT_SSH_PRIVATE_KEYFILE
	 */
	const SSH_PRIVATE_KEYFILE = 10153;

	/**
	 * Список шифров, используемый в SSL-передачах. Например, RC4-SHA и TLSv1 являются корректными списками шифров.
	 * @see CURLOPT_SSL_CIPHER_LIST
	 */
	const SSL_CIPHER_LIST = 10083;

	/**
	 * Имя файла с корректно отформатированным PEM-сертификатом.
	 * @see CURLOPT_SSLCERT
	 */
	const SSLCERT = 10025;

	/**
	 * Пароль, необходимый для использования сертификата @see CURLOPT_SSLCERT.
	 * @see CURLOPT_SSLCERTPASSWD
	 */
	const SSLCERTPASSWD = 10026;

	/**
	 * Формат сертификата. Поддерживаются форматы "PEM" (по умолчанию), "DER" и "ENG".
	 * @see CURLOPT_SSLCERTTYPE
	 */
	const SSLCERTTYPE = 10086;

	/**
	 * Идентификатор механизма шифрования для закрытого ключа SSL, указанного в параметре @see CURLOPT_SSLKEY.
	 * @see CURLOPT_SSLENGINE
	 */
	const SSLENGINE = 10089;

	/**
	 * Идентификатор механизма шифрования, используемого для ассиметричных операций шифрования.
	 * @see CURLOPT_SSLENGINE_DEFAULT
	 */
	const SSLENGINE_DEFAULT = 90;

	/**
	 * Имя файла с закрытым ключом SSL.
	 * @see CURLOPT_SSLKEY
	 */
	const SSLKEY = 10087;

	/**
	 * Тайный пароль, необходимый для использования закрытого ключа SSL, указанного параметром @see CURLOPT_SSLKEY.
	 * @see CURLOPT_SSLKEYPASSWD
	 */
	const SSLKEYPASSWD = 10026;

	/**
	 * Тип закрытого ключа SSL, указанного в параметре @see CURLOPT_SSLKEY.
	 * Поддерживаются следующие типы ключей: "PEM" (по умолчанию), "DER" и "ENG".
	 * @see CURLOPT_SSLKEYTYPE
	 */
	const SSLKEYTYPE = 10088;

	/**
	 * Разрешает использовать доменные сокеты UNIX в качестве конечной точки для соединения и устанавливает
	 * путь к ним равным заданной строке
	 * @see CURLOPT_UNIX_SOCKET_PATH
	 */
	const UNIX_SOCKET_PATH = 10231;

	/**
	 * Загружаемый URL. Данный параметр может быть также установлен при инициализации сеанса с помощью
	 * @see CURLOPT_URL
	 */
	const URL = 10002;

	/**
	 * Содержимое заголовка "User-Agent: ", посылаемого в HTTP-запросе.
	 * @see CURLOPT_USERAGENT
	 */
	const USERAGENT = 10018;

	/**
	 * Ипя пользователя для аутентификации.
	 * @see CURLOPT_USERNAME
	 */
	const USERNAME = 10173;

	/**
	 * Логин и пароль, используемые при соединении, указанные в формате "[username]:[password]".
	 * @see CURLOPT_USERPWD
	 */
	const USERPWD = 10005;

	/**
	 * Задает токен доступа OAuth 2.0.
	 * @see CURLOPT_XOAUTH2_BEARER
	 */
	const XOAUTH2_BEARER = 10220;

	/**
	 * Соединяться с указанный хостом по указанному порту, игнорируя URL.
	 * Принимает массив строк формата HOST:PORT:CONNECT-TO-HOST:CONNECT-TO-PORT.
	 * @see CURLOPT_CONNECT_TO
	 */
	const CONNECT_TO = 10243;

	/**
	 * Массив HTTP 200 ответов, которые будут трактоваться корректными ответами, а не ошибочными.
	 * @see CURLOPT_HTTP200ALIASES
	 */
	const HTTP200ALIASES = 10104;

	/**
	 * Массив устанавливаемых HTTP-заголовков, в формате array('Content-type: text/plain', 'Content-length: 100')
	 * @see CURLOPT_HTTPHEADER
	 */
	const HTTPHEADER = 10023;

	/**
	 * Массив FTP-команд, выполняемых на сервере, после выполнения FTP-запроса.
	 * @see CURLOPT_POSTQUOTE
	 */
	const POSTQUOTE = 10039;

	/**
	 * Массив пользовательских HTTP-заголовков для отправки на прокси
	 * @see CURLOPT_PROXYHEADER
	 */
	const PROXYHEADER = 10228;

	/**
	 * Массив FTP-команд, выполняемых на сервере, перед выполнением FTP-запроса.
	 * @see CURLOPT_QUOTE
	 */
	const QUOTE = 10028;

	/**
	 * Предоставляет адрес для определенной пары хоста и порта. Массив, содержащий строки состоящие из имени хоста,
	 * порта и IP-адреса, разделенных двоеточием. Пример: array("example.com:80:127.0.0.1")
	 * @see CURLOPT_RESOLVE
	 */
	const RESOLVE = 10203;

	/**
	 * Файл, в который будет записан результат передачи. По умолчанию используется поток вывода STDOUT (окно браузера).
	 * @see CURLOPT_FILE
	 */
	const FILE = 10001;

	/**
	 * Файл, из которого должно идти чтение данных, при загрузке на сервер.
	 * @see CURLOPT_INFILE
	 */
	const INFILE = 10009;

	/**
	 * Альтернативный файл для вывода ошибок, используемый вместо потока ошибок STDERR.
	 * @see CURLOPT_STDERR
	 */
	const STDERR = 10037;

	/**
	 * Файл, в который будут записаны заголовки текущей операции.
	 * @see CURLOPT_WRITEHEADER
	 */
	const WRITEHEADER = 10029;

	/*
	 * ---------------------
	 */

	/**
	 * Callback-функция принимает два параметра. Первым параметром является дескриптор cURL, вторым параметром
	 * является строка с записываемыми заголовками. Заголовки должны быть записаны с помощью данной callback-функции.
	 * Должна возвратить количество записанных байт.
	 * @see CURLOPT_HEADERFUNCTION
	 */
	const HEADERFUNCTION = 20079;

	/**
	 * Callback-функция принимает три параметра. Первым параметром является дескриптор cURL, вторым параметром является
	 * строка с запросом пароля, третьим параметром является максимальная длина пароля.
	 * Должна возвратить строку, содержащую пароль.
	 * @see CURLOPT_PASSWDFUNCTION
	 */
	const PASSWDFUNCTION = -1;

	/**
	 * Callback-функция принимает пять параметров. Первый является декскриптором cURL, второй - общим количеством байт,
	 * которое ожидается загрузить с сервера, третий - количество уже загруженных байт, четвертый - общее количество байт,
	 * которое ожидается отправить на сервер, и пятый - количество уже отправленных байт.
	 * Замечание:
	 * 	Callback-функция вызывается только, если опция @see CURLOPT_NOPROGRESS установлена в значение FALSE.
	 * Можно вернуть ненулевое значение, чтобы отменить передачу. В этом случае будет выдана ошибка @see CURLE_ABORTED_BY_CALLBACK.
	 * @see CURLOPT_PROGRESSFUNCTION
	 */
	const PROGRESSFUNCTION = 20056;

	/**
	 * Callback-функция принимает три параметра. Первым параметром является дескриптор cURL, вторым параметром является
	 * ресурс потока, переданный cURL через опцию CURLOPT_INFILE, а третьим параметром является максимально разрешенное
	 * количество данных для чтения. Callback-функция должна возвратить строку с длиной, не превышающей запрошенного
	 * количества данных, обычно с помощью чтения из переданного потокового ресурса. Должна возвратить пустую строку
	 * для сигнала о конце файла EOF.
	 * @see CURLOPT_READFUNCTION
	 */
	const READFUNCTION = 20012;

	/**
	 * Callback-функция принимает два параметра. Первым параметром является дескриптор cURL, а вторым параметром
	 * является строка с записываемыми данными. Данные должны быть сохранены с помощью данной функции. Она должна
	 * возвратить точное количество записанных байт, иначе закачка будет прервана с ошибкой.
	 * @see CURLOPT_WRITEFUNCTION
	 */
	const WRITEFUNCTION = 20011;

	/**
	 * Результат выполнения функции curl_share_init(). Позволяет обработчику cURL использовать данные из общего обработчика.
	 * @see CURLOPT_SHARE
	 */
	const SHARE = 10100;
}
<?php
namespace Keepper\Lib\Curl\Option;

use Keepper\Lib\Curl\Exceptions\IllegalOptionValueException;

class OptionValidator implements OptionValidatorInterface {

	/**
	 * @inheritdoc
	 */
	public function isOptionValueValid(int $option, $value): bool {
		try {
			$this->throwIsOptionValueInvalid($option, $value);
			return true;
		} catch (\Exception $e) {
			return false;
		}
	}

	/**
	 * @inheritdoc
	 */
	public function throwIsOptionValueInvalid(int $option, $value) {

		if ( in_array($option, Option::BOOLEAN_VALUES) ) {
			if ( !(is_bool($value) || in_array($value, [0,1])) ) {
				throw new IllegalOptionValueException('Значение должно быть типа boolean', $option);
			}
			return;
		}

		if ( in_array($option, Option::INTEGER_VALUES) ) {
			if ( !is_numeric($value) ) {
				throw new IllegalOptionValueException('Значение должно быть типа integer', $option);
			}
			return;
		}

		if ( in_array($option, Option::STRING_VALUES) ) {
			if ( !is_string($value) ) {
				throw new IllegalOptionValueException('Значение должно быть типа string', $option);
			}
			return;
		}

		if ( in_array($option, Option::ARRAY_VALUES) ) {
			if ( !is_array($value) ) {
				throw new IllegalOptionValueException('Значение должно быть типа array', $option);
			}
			return;
		}

		if ( in_array($option, Option::DESCRIPTOR_VALUES) ) {
			if ( !is_resource($value) ) {
				throw new IllegalOptionValueException('Значение должно быть потоковым дескриптором типа resource', $option);
			}
			return;
		}

		/*if ( in_array($option, Option::CALLABLE_VALUES) ) {
			if ( !is_callable($value) ) {
				throw new IllegalOptionValueException('Значение должно быть именем функции или замыканием', $option);
			}
			return;
		}*/
	}
}
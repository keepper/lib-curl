<?php
namespace Keepper\Lib\Curl\Option;

use Keepper\Lib\Curl\Exceptions\IllegalOptionValueException;

interface OptionValidatorInterface {

	/**
	 * Возвращает признак того, что значение для указаной опции валидно
	 * @param int $option
	 * @param $value
	 * @return bool
	 */
	public function isOptionValueValid(int $option, $value): bool;

	/**
	 * Проверяет что значение для указаной опции валидно, в случае не валидности генерирует исключение
	 * @param int $option
	 * @param $value
	 * @return mixed
	 * @throws IllegalOptionValueException
	 */
	public function throwIsOptionValueInvalid(int $option, $value);
}
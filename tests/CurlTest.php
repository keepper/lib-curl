<?php
namespace Keepper\Lib\Curl\Tests;

use Keepper\Lib\Curl\Curl;
use Keepper\Lib\Curl\CurlInterface;
use Keepper\Lib\Curl\Option\Option;
use Monolog\Logger;
use Psr\Log\LoggerInterface;

class CurlTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @var LoggerInterface
	 */
	private $logger;

	public function setUp() {
		parent::setUp();
		$this->logger = new Logger('UnitTests');
	}

	public function testInitWithoutUrl() {
		/**
		 * @var CurlInterface|\PHPUnit_Framework_MockObject_MockObject $curl
		 */
		$curl = $this->getMockBuilder(Curl::class)
			->setConstructorArgs([null, $this->logger])
			->setMethods(['curl_init', 'curl_close'])
			->getMock();

		$curl->expects($this->never())->method('curl_close');
		$curl->expects($this->once())->method('curl_init')->with();

		$curl->init();
	}

	public function testInitWithUrl() {
		$url = 'http://php.net/manual/ru/function.curl-setopt.php';
		/**
		 * @var CurlInterface|\PHPUnit_Framework_MockObject_MockObject $curl
		 */
		$curl = $this->getMockBuilder(Curl::class)
			->setConstructorArgs([null, $this->logger])
			->setMethods(['curl_init', 'curl_close'])
			->getMock();

		$curl->expects($this->never())->method('curl_close');
		$curl->expects($this->once())->method('curl_init')->with($url);

		$curl->init($url);
	}

	public function testInitClosePreviuse() {
		$descr = rand(1,100);

		/**
		 * @var CurlInterface|\PHPUnit_Framework_MockObject_MockObject $curl
		 */
		$curl = $this->getMockBuilder(Curl::class)
			->setConstructorArgs([null, $this->logger])
			->setMethods(['curl_init', 'curl_close'])
			->getMock();

		$curl->method('curl_init')->with()->willReturn($descr);

		$curl->init();

		$curl->expects($this->once())->method('curl_close')->with($descr);
		$curl->init();
	}

	/**
	 * @dataProvider dataProviderForThrowingExcheptionOnCallMethodsWithoutInit
	 * @expectedException \Keepper\Lib\Curl\Exceptions\IllegalOperation
	 * @expectedExceptionMessage Вызов зависимого метода без инициализации дескриптора
	 */
	public function testThrowingExcheptionOnCallMethodsWithoutInit($methodName, $args) {
		$curl = new Curl(null, $this->logger);
		call_user_func_array([$curl, $methodName], $args);
	}

	public function dataProviderForThrowingExcheptionOnCallMethodsWithoutInit() {
		return [
			['setOption', [12345678, 'some value']],
			['setOptions', [[]]],
			['escape', ['some string']],
			['unescape', ['some string']],
			['error', []],
			['exec', []],
			['getInfo', [1]]
		];
	}

	/**
	 * @dataProvider dataProviderForThrowValidateExceptionOnSetOption
	 * @expectedException  \Keepper\Lib\Curl\Exceptions\IllegalOptionValueException
	 */
	public function testThrowValidateExceptionOnSetOption($optionName, $value) {
		$curl = new Curl(null, $this->logger);
		$curl->init();
		$curl->setOption($optionName, $value);
	}

	public function dataProviderForThrowValidateExceptionOnSetOption() {
		return [
			[Option::INTEGER_VALUES[0], 'abc']
		];
	}

	/**
	 * @dataProvider dataProviderForSetOption
	 */
	public function testSetOption($expectedResult) {
		$descr = rand(1,100);
		/**
		 * @var CurlInterface|\PHPUnit_Framework_MockObject_MockObject $curl
		 */
		$curl = $this->getMockBuilder(Curl::class)
			->setConstructorArgs([null, $this->logger])
			->setMethods(['curl_init', 'curl_setopt'])
			->getMock();

		$curl->method('curl_init')->with()->willReturn($descr);
		$curl->expects($this->once())
			->method('curl_setopt')
			->with($descr, Option::URL, 'http://php.net/')
			->willReturn($expectedResult);

		$curl->init();
		$result = $curl->setOption(Option::URL, 'http://php.net/');
		$this->assertEquals($expectedResult, $result, 'Ожидали, что метод вернет тотже результат, что и дочерний curl_setopt');
	}

	public function dataProviderForSetOption() {
		return [
			[true],
			[false]
		];
	}

	/**
	 * @dataProvider dataProxyMethods
	 */
	public function testProxyMethods($method, $proxy, $arg, $expectedResult) {
		$descr = rand(1,100);
		/**
		 * @var CurlInterface|\PHPUnit_Framework_MockObject_MockObject $curl
		 */
		$curl = $this->getMockBuilder(Curl::class)
			->setConstructorArgs([null, $this->logger])
			->setMethods(['curl_init', $proxy])
			->getMock();

		$curl->method('curl_init')->with()->willReturn($descr);
		$curl->expects($this->once())
			->method($proxy)
			->with($descr, $arg)
			->willReturn($expectedResult);

		$curl->init();
		$result = $curl->$method($arg);
		$this->assertEquals($expectedResult, $result, 'Ожидали, что метод вернет тотже результат, что и дочерний '.$proxy);
	}

	public function dataProxyMethods() {
		return [
			['escape', 'curl_escape', 'some string', 'some result '.rand(1, 1000)],
			['unescape', 'curl_unescape', 'some string', 'some result '.rand(1, 1000)],
			['getInfo', 'curl_getinfo', Option::URL, rand(0,1000)]
		];
	}

	public function testGetNullableError() {
		$descr = rand(1,100);
		/**
		 * @var CurlInterface|\PHPUnit_Framework_MockObject_MockObject $curl
		 */
		$curl = $this->getMockBuilder(Curl::class)
			->setConstructorArgs([null, $this->logger])
			->setMethods(['curl_init', 'curl_errno'])
			->getMock();

		$curl->method('curl_init')->with()->willReturn($descr);
		$curl->method('curl_errno')->with($descr)->willReturn(null);

		$curl->init();
		$result = $curl->error();
		$this->assertNull($result);
	}

	/**
	 * @dataProvider dataProviderForGetError
	 */
	public function testGetError($code, $message, $msgByCode) {
		$descr = rand(1,100);
		/**
		 * @var CurlInterface|\PHPUnit_Framework_MockObject_MockObject $curl
		 */
		$curl = $this->getMockBuilder(Curl::class)
			->setConstructorArgs([null, $this->logger])
			->setMethods(['curl_init', 'curl_errno', 'curl_error', 'curl_strerror'])
			->getMock();

		$curl->method('curl_init')->with()->willReturn($descr);
		$curl->method('curl_errno')->with($descr)->willReturn($code);
		$curl->method('curl_error')->with($descr)->willReturn($message);
		$curl->method('curl_strerror')->with($code)->willReturn($msgByCode);

		$curl->init();
		$result = $curl->error();
		$this->assertEquals($code, $result->code());
		$this->assertEquals($message, $result->message());
		$this->assertEquals($msgByCode, $result->messageByCode());
	}

	public function dataProviderForGetError() {
		return [
			[1, 'Some message', 'Some message by code'],
			[2, 'Some message 2', 'Some message by code 2'],
		];
	}

	/**
	 * @dataProvider dataProviderForSetOptions
	 */
	public function testSetOptions($firstResult, $secondResult, $expectedResult) {
		$descr = rand(1,100);

		/**
		 * @var CurlInterface|\PHPUnit_Framework_MockObject_MockObject $curl
		 */
		$curl = $this->getMockBuilder(Curl::class)
			->setConstructorArgs([null, $this->logger])
			->setMethods(['curl_init', 'setOption', 'curl_setopt'])
			->getMock();

		$curl->method('curl_init')->with()->willReturn($descr);
		$curl->expects($this->never())->method('curl_setopt');
		$curl->method('setOption')->will($this->returnValueMap([
			[Option::URL, 'http://php.net/ru/', $firstResult],
			[Option::REFERER, 'http://php.net/', $secondResult],
		]));

		$curl->init();
		$result = $curl->setOptions([
			Option::URL => 'http://php.net/ru/',
			Option::REFERER => 'http://php.net/'
		]);

		$this->assertEquals($expectedResult, $result);
	}

	public function dataProviderForSetOptions() {
		return [
			[true, true, true],
			[true, false, false],
			[false, true, false],
			[false, false, false]
		];
	}

	public function testReset() {
		$descr = rand(1,100);
		/**
		 * @var CurlInterface|\PHPUnit_Framework_MockObject_MockObject $curl
		 */
		$curl = $this->getMockBuilder(Curl::class)
			->setConstructorArgs([null, $this->logger])
			->setMethods(['curl_init', 'curl_reset'])
			->getMock();

		$curl->method('curl_init')->with()->willReturn($descr);
		$curl->expects($this->once())->method('curl_reset')->with($descr);

		$curl->init();
		$result = $curl->reset();
		$this->assertEquals($curl, $result);
	}

	public function testResetWithoutDescriptor() {
		/**
		 * @var CurlInterface|\PHPUnit_Framework_MockObject_MockObject $curl
		 */
		$curl = $this->getMockBuilder(Curl::class)
			->setConstructorArgs([null, $this->logger])
			->setMethods(['curl_reset'])
			->getMock();

		$curl->expects($this->never())->method('curl_reset');

		$result = $curl->reset();
		$this->assertEquals($curl, $result);
	}

	/**
	 * @dataProvider dataProviderForExecPositive
	 */
	public function testExecPositive($execResult, $expectedResult) {
		$descr = rand(1,100);
		/**
		 * @var CurlInterface|\PHPUnit_Framework_MockObject_MockObject $curl
		 */
		$curl = $this->getMockBuilder(Curl::class)
			->setConstructorArgs([null, $this->logger])
			->setMethods(['curl_init', 'curl_exec'])
			->getMock();

		$curl->method('curl_init')->with()->willReturn($descr);
		$curl->expects($this->once())
			->method('curl_exec')
			->with($descr)
			->willReturn($expectedResult);

		$curl->init();
		$result = $curl->exec();

		$this->assertEquals($expectedResult, $result);
	}

	public function dataProviderForExecPositive() {
		return [
			[true, ''],
			['some content', 'some content']
		];
	}

	/**
	 * @expectedException \Keepper\Lib\Curl\Exceptions\CurlException
	 * @expectedExceptionMessage Типа ошибка
	 */
	public function testExecNegative() {
		$descr = rand(1,100);
		/**
		 * @var CurlInterface|\PHPUnit_Framework_MockObject_MockObject $curl
		 */
		$curl = $this->getMockBuilder(Curl::class)
			->setConstructorArgs([null, $this->logger])
			->setMethods(['curl_init', 'curl_errno', 'curl_error', 'curl_strerror', 'curl_exec'])
			->getMock();

		$curl->method('curl_init')->with()->willReturn($descr);
		$curl->method('curl_errno')->with($descr)->willReturn(1);
		$curl->method('curl_error')->with($descr)->willReturn('Типа ошибка');
		$curl->method('curl_strerror')->with(1)->willReturn('Типа ошибка по коду');
		$curl->expects($this->once())->method('curl_exec')->with($descr)->willReturn(false);

		$curl->init();
		$curl->exec();
	}

	/**
	 * @group integration
	 */
	public function testIntegration() {
		$curl = new Curl(null, $this->logger);
		$curl->init('http://php.net/');
		$curl->setOption(Option::RETURNTRANSFER, true);
		$result = $curl->exec();
		$this->assertEquals('<!DOC', substr($result, 0, 5));
	}
}


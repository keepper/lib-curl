<?php
namespace Keepper\Lib\Curl\Tests\Error;

use Keepper\Lib\Curl\Error\CurlError;
use Keepper\Lib\Curl\Error\CurlErrorConstsInterface;

class CurlErrorTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @dataProvider dataProviderForCurlError
	 */
	public function testCurlError($code, $message, $codeMessage) {
		$error = new CurlError($code, $message, $codeMessage);

		$this->assertEquals($code, $error->code());
		$this->assertEquals($message, $error->message());
		$this->assertEquals($codeMessage, $error->messageByCode());
	}

	public function dataProviderForCurlError() {
		return [
			[1, 'Some message', 'Some message code'],
			[2, 'Some other message', '']
		];
	}

	public function testCurlConstValues() {
        foreach (CurlErrorConstsInterface::MAP as $value => $phpConstantName) {
            if ( !defined($phpConstantName) || constant($phpConstantName) == -1) {
                continue;
            }

            $this->assertEquals($value, constant($phpConstantName), 'Ожидали совпадения значения константы с php константой '.$phpConstantName);
	    }
    }
}
<?php
namespace Keepper\Lib\Curl\Tests\Option;

use Keepper\Lib\Curl\Option\Option;

class OptionTest extends \PHPUnit_Framework_TestCase {

	public function testConstantValues() {
		foreach (Option::MAP as $value => $phpConstantName) {
			if ( !defined($phpConstantName) || constant($phpConstantName) == -1) {
				continue;
			}

			$this->assertEquals($value, constant($phpConstantName), 'Ожидали совпадения значения константы с php константой '.$phpConstantName);
		}
	}
}
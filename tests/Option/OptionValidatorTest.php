<?php
namespace Keepper\Lib\Curl\Tests\Option;

use Keepper\Lib\Curl\Option\Option;
use Keepper\Lib\Curl\Option\OptionValidator;

class OptionValidatorTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @dataProvider dataProviderForTestPositive
	 */
	public function testPositive($option, $value, $optionName) {
		$validator = new OptionValidator();
		$this->assertTrue($validator->isOptionValueValid($option, $value), 'Ожидали, что устанавливаемое значение "'.print_r($value, true).'" опции '.$optionName.' корректно');
		// не должно быть исключений
		$validator->throwIsOptionValueInvalid($option, $value);
	}

	public function dataProviderForTestPositive() {
		$data = [];
		foreach (Option::BOOLEAN_VALUES as $code) {
			$data[] = [$code, rand(0,1), Option::MAP[$code]];
		}

		foreach (Option::INTEGER_VALUES as $code) {
			$data[] = [$code, rand(0,100000), Option::MAP[$code]];
		}

		$s = ['abc', 'this is are string'];
		foreach (Option::STRING_VALUES as $code) {
			$data[] = [$code, $s[rand(0,1)], Option::MAP[$code]];
		}

		foreach (Option::ARRAY_VALUES as $code) {
			$data[] = [$code, [], Option::MAP[$code]];
		}

		$f = fopen('/tmp/123', 'w');
		foreach (Option::DESCRIPTOR_VALUES as $code) {
			$data[] = [$code, $f, Option::MAP[$code]];
		}

		return $data;
	}
}